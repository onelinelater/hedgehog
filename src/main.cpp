#include "modules/cli-interface.h"
#include <iostream>
#include <windows.h>

using namespace std;

int main (int argc, char *argv[]) {
	SetConsoleCP(CP_UTF8);
    SetConsoleOutputCP(CP_UTF8);
    Cli *cli = new Cli();
    cli->run(argc, argv);
    delete cli;
	return 0;
}
