#include <utility>
#include <string>
#include <iostream>
#include "cli-interface.h"
#include "lzw.h"

using namespace std;

Command::Command(Cli *_parent) {
    parent = _parent;
}

Command::~Command() {
    this->parent = nullptr;
}

void Command::execute() {}

bool Command::parse_args(int, char **) { return true; }

class Help : public Command{
    public:
        explicit Help(Cli *parent) : Command(parent) {
            name = "help";
            comment = "show commands and comments about";
        }

        void execute() override {
            Cli *parent = this->parent;
            for (auto const& [key, val] : parent->commands){
                cout << "[" << key << "] - " << val->comment << std::endl;
            }
        }
};

class Compress : public Command{
    public:
        explicit Compress(Cli *parent) : Command(parent) {
            name = "compress";
            comment = "make file smaller";
        }

        void execute() override {
            compress(this->parent->input_path, this->parent->output_path);
        }

        bool parse_args(int argc, char **argv) override {
            if (argc < 4) {
                this->parent->input_path = "./input.txt";
                this->parent->output_path = "./output.lzw";
            } else {
                this->parent->input_path = argv[2];
                this->parent->output_path = argv[3];
            }

            return true;
        }
};


class Decompress : public Command{
    public:
        explicit Decompress(Cli *parent) : Command(parent) {
            name = "decompress";
            comment = "restore file size";
        }

        void execute() override {
            decompress(this->parent->input_path, this->parent->output_path);
        }

        bool parse_args(int argc, char **argv) override {
            if (argc < 4) {
                this->parent->input_path = "./output.lzw";
                this->parent->output_path = "./decompressed.txt";
            } else {
                this->parent->input_path = argv[2];
                this->parent->output_path = argv[3];
            }

            return true;
        }
};

Cli::Cli () {
    this->add_command(new Help(this));
    this->add_command(new Compress(this));
    this->add_command(new Decompress(this));
}

Cli::~Cli() {
    for (auto const& [key, val] : this->commands){
        delete val;
    }
}

void Cli::run (int argc, char **argv) {
    if (this->parse_args(argc, argv)) {
        this->commands[this->command]->execute();
    }
}

void Cli::add_command(Command *new_command) {
    this->commands[new_command->name] = new_command;
}

bool Cli::parse_args(int argc, char **argv) {
    if (argc < 2) {
        cout << "Command is not specified!";
        return false;
    }
    if (this->commands.find(argv[1]) == this->commands.end() ) {
        cout << "Command is not found. Use [help] to see all commands.";
        return false;
    } else {
        this->command = argv[1];
    }
    return this->commands[this->command]->parse_args(argc, argv);
}
