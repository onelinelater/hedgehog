#ifndef COMPRESS_CLI_INTERFACE_H
#define COMPRESS_CLI_INTERFACE_H

#include <map>
#include <string>


using namespace std;

class Cli;

class Command {
public:
    Cli *parent = nullptr;

    explicit Command(Cli *parent);
    ~Command();

    virtual void execute();
    virtual bool parse_args(int,  char**);

    string name;
    string comment;
};

class Cli {
    public:
        map<string, Command*> commands;
        string command;
        string input_path;
        string output_path;

        Cli ();
        ~Cli ();
        void run (int argc, char *argv[]);
        bool parse_args (int argc, char *argv[]);
        void add_command(Command *);
};
#endif
