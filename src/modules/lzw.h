#ifndef COMPRESS_ENCODE_DECODE_H
#define COMPRESS_ENCODE_DECODE_H
#include <string>

    using namespace std;

    void compress(const string&, const string&);
    void decompress(const string& , const string&);
#endif
