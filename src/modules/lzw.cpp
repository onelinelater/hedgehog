#include <string>
#include <map>
#include <iostream>
#include <vector>
#include <fstream>
#include <sys\stat.h>
#include "lzw.h"

using namespace std;

int FileSize(const string& szFileName )
{
    struct stat fileStat{};
    int err = stat( szFileName.c_str(), &fileStat );
    if (0 != err) return 0;
    return fileStat.st_size;
}

template <typename Iterator>
Iterator compress_file(ifstream &uncompressed, Iterator result) {
    int dictSize = 256;
    map<string, int> dictionary;
    for (int i = 0; i < 256; i++)
        dictionary[string(1, i)] = i;

    string w;
    char c;
    while (uncompressed.get(c)) {
        string wc = w + c;
        if (dictionary.count(wc))
            w = wc;
        else {
            *result++ = dictionary[w];
            dictionary[wc] = dictSize++;
            w = string(1, c);
        }
    }

    if (!w.empty())
        *result++ = dictionary[w];
    return result;
}

string decompress_file(ifstream &compressed) {
    int dictSize = 256;
    map<int,string> dictionary;
    for (int i = 0; i < 256; i++)
        dictionary[i] = string(1, i);

    short c;
    compressed.read(reinterpret_cast<char *>(&c), sizeof(c));
    string w(1, c);
    string result = w;
    string entry;
    while (compressed.read(reinterpret_cast<char *>(&c), sizeof(c))) {
        if (dictionary.count(c))
            entry = dictionary[c];
        else if (c == dictSize) {
            entry = w + w[0];
        } else {
            cout << "Broken compression!";
        }

        result += entry;

        dictionary[dictSize++] = w + entry[0];

        w = entry;
    }
    return result;
}


void compress(const string& input_path, const string& output_path) {
    vector<short> compressed;

    ifstream input (input_path, ios::in | ios::binary);

    if (input.is_open()) {
        compress_file(input, back_inserter(compressed));
        input.close();
    } else {
        cout << "Unable to open file " << input_path << endl;
        return;
    }

    ofstream output;
    output.open (output_path, ios::out | ios::binary);

    if (output.is_open()) {
        for(short  i : compressed) {
            output.write(reinterpret_cast<const char *>(&i), sizeof(short ));
        }
        output.close();
        cout << "size of input " << FileSize(input_path) << " bytes" << endl;
        cout << "size of output " << FileSize(output_path) << " bytes" << endl;
    } else {
        cout << "Unable to open file" << output_path << endl;
        return;
    }
}

void decompress(const string& input_path, const string& output_path) {
    ifstream input(input_path, ios::in | ios::binary);
    string decompressed;

    if (input.is_open()) {
        decompressed = decompress_file(input);
        input.close();
    } else {
        cout << "Unable to open file " << input_path << endl;
        return;
    }

    ofstream output;
    output.open (output_path, ios::out | ios::binary);

    if (output.is_open()) {
        output << decompressed;
        output.close();
        cout << "size of input " << FileSize(input_path) << " bytes" << endl;
        cout << "size of output " << FileSize(output_path) << " bytes" << endl;
    } else {
        cout << "Unable to open file " << output_path << endl;
        return;
    }

}

