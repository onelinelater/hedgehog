cmake_minimum_required(VERSION 3.17)
project(hedgehog)

set(CMAKE_CXX_STANDARD 20)

set(APP_ICON_RESOURCE_WINDOWS "src/res/main.rc")
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ..)

add_executable(hedgehog src/main.cpp src/modules/cli-interface.h src/modules/cli-interface.cpp src/modules/lzw.h src/modules/lzw.cpp ${APP_ICON_RESOURCE_WINDOWS})
